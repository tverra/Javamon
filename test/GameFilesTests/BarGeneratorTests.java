package GameFilesTests;

import GUI.BarProperty;
import GameFiles.BarGenerator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class BarGeneratorTests {
    private BarGenerator barGen;
    private Monsters.Monster monster;
    private BarProperty[] hpBar;

    private final int NUMBER_OF_PIECES = 21;
    private final String FX1 = "-fx-background-color: ";
    private final String HIGH_HP = "green";
    private final String MIDDLE_HP = "orange";
    private final String LOW_HP = "red";
    private final String NO_HP = "white";
    private final String FX2 = "; -fx-border-color: black";

    @Before
    public void setUp() {
        monster = mock(Monsters.Monster.class);
        barGen = new BarGenerator();
        when(monster.getHP()).thenReturn(100);
        hpBar = new BarProperty[NUMBER_OF_PIECES];

        for (int i = 0; i < NUMBER_OF_PIECES; i++) {
            hpBar[i] = new BarProperty();
        }
    }

    @Test
    public void monsterHasFullHp_PiecesAreGreen() {
        when(monster.getRemainingHP()).thenReturn(100);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (BarProperty barPart : hpBar) {
            assertEquals(FX1+HIGH_HP+FX2, barPart.getBarPart());
        }
    }

    @Test
    public void monsterHasLostOneHp_FirstPieceIsWhite() {
        when(monster.getRemainingHP()).thenReturn(99);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i < NUMBER_OF_PIECES - 1; i++) { assertEquals(FX1+HIGH_HP+FX2, hpBar[i].getBarPart());
        }
        assertEquals(FX1+NO_HP+FX2, hpBar[NUMBER_OF_PIECES - 1].getBarPart());
    }

    @Test
    public void monsterHasOverHalfHp_OneLessThanHalfIsGreen() {
        when(monster.getRemainingHP()).thenReturn(51);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i <= NUMBER_OF_PIECES / 2; i++) {
            assertEquals(FX1+HIGH_HP+FX2, hpBar[i].getBarPart());
        }
        for (int i = (NUMBER_OF_PIECES / 2) + 1; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());
        }
    }

    @Test
    public void monsterHasHalfHp_OneLessThanHalfIsOrange() {
        when(monster.getRemainingHP()).thenReturn(50);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i <= NUMBER_OF_PIECES / 2; i++) {
            assertEquals(FX1+MIDDLE_HP+FX2, hpBar[i].getBarPart());
        }
        for (int i = (NUMBER_OF_PIECES / 2) + 1; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());
        }
    }

    @Test
    public void monsterHasMoreThanOneFifthHp_OneLessThanOneFifthIsOrange() {
        when(monster.getRemainingHP()).thenReturn(21);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i <= NUMBER_OF_PIECES / 5; i++) {
            assertEquals(FX1+MIDDLE_HP+FX2, hpBar[i].getBarPart());
        }
        for (int i = (NUMBER_OF_PIECES / 5) + 1; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());

        }
    }

    @Test
    public void monsterHasOneFifthHp_OneLessThanOneFifthIsRed() {
        when(monster.getRemainingHP()).thenReturn(20);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i <= NUMBER_OF_PIECES / 5; i++) {
            assertEquals(FX1+LOW_HP+FX2, hpBar[i].getBarPart());
        }
        for (int i = (NUMBER_OF_PIECES / 5) + 1; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());
        }
    }

    @Test
    public void monsterHasOneHpLeft_OnePieceIsRed() {
        when(monster.getRemainingHP()).thenReturn(1);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        assertEquals(FX1+LOW_HP+FX2, hpBar[0].getBarPart());
        for (int i = 1; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());
        }
    }

    @Test
    public void monsterHasNoHpLeft_AllPiecesAreWhite() {
        when(monster.getRemainingHP()).thenReturn(0);
        hpBar = barGen.setHpBarColor(monster, hpBar);

        for (int i = 0; i < NUMBER_OF_PIECES; i++) {
            assertEquals(FX1+NO_HP+FX2, hpBar[i].getBarPart());
        }
    }

    @Test(expected = NullPointerException.class)
    public void monsterObjectIsNull_throwsNullPointerException() {
        hpBar = barGen.setHpBarColor(null, hpBar);
    }

    @Test
    public void monsterRemainingHpIsNull_returnsNull() {
        for (int i = 0; i < NUMBER_OF_PIECES; i++) {
            assertEquals(null, hpBar[i].getBarPart());
        }
    }

    @Test(expected = NullPointerException.class)
    public void hpBarObjectIsNull_throwsNullPointerException() {
        hpBar = barGen.setHpBarColor(monster, null);
    }
}