package Moves;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class FireMove {

    public Move firePunch = new Move("FIRE PUNCH", "fire", "physical", true,
            "A fiery punch that may burn the foe.", 75, 100,
            new ArrayList<>(Collections.singletonList("burn")),
            new ArrayList<>(Collections.singletonList(10)), 15);

    public Move flareBlitz = new Move("FLARE BLITZ", "fire", "physical", true,
            "A fiery tackle that also hurts the user.", 120, 100,
            new ArrayList<>(Arrays.asList("burn", "thawUser", "recoilDamageDone")),
            new ArrayList<>(Arrays.asList(10, 100, 33)), 15);

    public Move flameCharge = new Move("FLAME CHARGE", "fire", "physical", true,
            "A flaming charge that raises speed.", 50, 100,
            new ArrayList<>(Collections.singletonList("raiseUserSpeed")),
            new ArrayList<>(Collections.singletonList(100)), 20);

    public Move willOWisp = new Move("WILL-O-WISP", "fire", "status", false,
            "Inflicts a burn on the foe with intense fire.", 0, 85,
            new ArrayList<>(Collections.singletonList("burn")),
            new ArrayList<>(Collections.singletonList(100)), 15);
}
