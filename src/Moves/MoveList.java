package Moves;

public class MoveList {
    public Move getMoveByName(String name, int ppUp) {
        Move move;
        name = name.toLowerCase();

        if(name.equals("")) {
            return new Move();
        }

        if(name.equals("bodyslam")) {
            move = new NormalMove().bodySlam;
            move.setPpUp(ppUp);
            return move;
        }

        else if(name.contains("firepunch")) {
            move = new FireMove().firePunch;
            move.setPpUp(ppUp);
            return move;
        }
        else if(name.contains("flareblitz")) {
            move = new FireMove().flareBlitz;
            move.setPpUp(ppUp);
            return move;
        }
        else if(name.contains("flamecharge")) {
            move = new FireMove().flameCharge;
            move.setPpUp(ppUp);
            return move;
        }
        else if(name.contains("will-o-wisp")) {
            move = new FireMove().willOWisp;
            move.setPpUp(ppUp);
            return move;
        }

        else {
            System.out.println("Error on loading move by name: " + name);
            return new Move();
        }
    }
}
