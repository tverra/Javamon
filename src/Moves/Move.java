package Moves;

import java.util.ArrayList;

public class Move {

    public Move() {
    }

    private String name = "";
    private String type = "";
    private String category = "";
    private boolean contact = true;
    private String description = "";
    private int power = 0;
    private int accuracy = 0;
    private ArrayList<String> effect = new ArrayList<>();
    private ArrayList<Integer> chances = new ArrayList<>();
    private int pp = 0;

    public Move(String name, String type, String category, boolean contact, String description,
                int power, int accuracy, ArrayList<String> effect, ArrayList<Integer> chances,
                int pp) {
        if(name.length() < 14 && !name.equals(""))
            this.name = name;
        else {
            System.out.println("Invalid move name: " + name);
        }

        String types = "normal fire fighting water flying grass poison electric ground " +
                "psychic rock ice bug dragon ghost dark steel fairy ???";
        if(types.contains(type.toLowerCase()))
            this.type = type.toLowerCase();
        else
            System.out.println("Invalid move type: " + type);

        if(category.toLowerCase().equals("physical") ||
                category.toLowerCase().equals("special") ||
                category.toLowerCase().equals("status")) {
            this.category = category.toLowerCase();
        }else {
            System.out.println("Invalid move category: " + category);
        }

        this.contact = contact;

        if(!description.equals("") && description.length() < 51) {
            this.description = description;
        }else {
            System.out.println("Invalid description length: " + description);
        }

        if(power >= 0 && power < 300) {
            this.power = power;
        }else {
            System.out.println("Invalid move power value: " + power);
        }

        if(accuracy >= 0 && accuracy <= 100) {
            this.accuracy = accuracy;
        }else {
            System.out.println("Invalid move accuracy value: " + accuracy);
        }

        this.effect = effect;

        int valueError = 0;
        for (Integer chance : chances) {
            if(chance < 0 && chance > 100) {
                System.out.println("Invalid chance value: " + chance);
                valueError = 1;
            }
        }
        if(valueError == 0) {
            this.chances = chances;
        }

        if(pp >= 0 && pp <= 45) {
            this.pp = pp;
        }else {
            System.out.println("Invalid pp value: " + pp);
        }
    }

    public String getName() {
        return name.toUpperCase();
    }
    public String getType() {
        return type;
    }
    public String getCategory() {
        return category;
    }
    public boolean getContact() {
        return contact;
    }
    public String getDescription() {
        return description;
    }
    public int getPower() {
        return power;
    }
    public int getAccuracy() {
        return accuracy;
    }
    public ArrayList<String> getEffect() {
        return effect;
    }
    public ArrayList<Integer> getChances() {
        return chances;
    }
    public int getPp(int ppUps) {
        return pp + ppUps;
    }

    public boolean subtractPp(int pp) {
        if(this.pp >= pp) {
            this.pp -= pp;
            return true;
        }else if(pp > 0) {
            this.pp --;
            return true;
        }
        return false;
    }
    public void setPpUp(int ppUp) {
        if(ppUp <= 3 && ppUp >= 0) {
            this.pp += ((this.pp / 5) * ppUp);
        }else {
            System.out.println("Invalid ppUp value: " + ppUp);
        }
    }
}
