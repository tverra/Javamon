package Moves;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class NormalMove extends Move {

    public NormalMove() {
    }

    public Move bodySlam = new Move("BODY SLAM", "normal", "physical", true,
            "A full-body slam that may cause paralysis.", 85, 100,
            new ArrayList<String>(Collections.singletonList("paralyze")),
            new ArrayList<Integer>(Collections.singleton(30)), 15);
}
