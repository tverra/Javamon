package Players;

import Monsters.Anders;

public class TestPlayer extends Player{
    public TestPlayer() {
        super.setName("Test");
        super.setGender("male");
        super.setIdNumber("1234");
        super.setMonster(new Anders("anders", 'M', "primary", "modest", 0,
                100000, new int[]{23, 11, 10, 1, 9, 12}, new int[]{0, 0, 0, 0, 0, 0},
                new String[]{"bodyslam", "", "", ""}, new int[]{0, 0, 0, 0}, "12345",
                "Testesen", 0, 30, 1, "@ home",
                100, ""), 0);
    }
}