package Players;

import Monsters.Monster;

public class Player {

    private String name = "";
    private String gender = "apache";
    private String idNumber = "";
    private Monster[] monsters = new Monster[]{new Monster(), new Monster(), new Monster(), new Monster(),
            new Monster(), new Monster()};

    public String getName() {
        return name;
    }
    public char getGender() {
        switch(gender.toLowerCase()) {
            case "male":
                return 'M';
            case "female":
                return 'F';
            case "apache":
                return 'A';
            default: {
                System.out.println("Error on getting gender on " + getName() + " : " + gender);
                return 'A';
            }
        }
    }
    public String getIdNumber() {
        return idNumber;
    }
    public Monster[] getMonsters() {
        return monsters;
    }

    public boolean setName(String name) {
        if (name.length() > 0 && name.length() < 16)
            this.name = name;
        else {
            System.out.println("Name length out of range: " + name);
            this.name = "";
            return false;
        }
        return true;
    }
    public boolean setGender(String gender) {
        String genders = "male female apache";
        if(genders.contains(gender.toLowerCase()))
            this.gender = gender.toLowerCase();
        else {
            System.out.println("Error on setting gender on " + getName() + ": " + gender);
            this.gender = "apache";
            return false;
        }
        return true;
    }
    public boolean setIdNumber(String idNumber) {
        if(Integer.parseInt(idNumber) > 0 && Integer.parseInt(idNumber) < 65536) {
            this.idNumber = idNumber;
        }else {
            System.out.println("Id number out of range on " + getName() + ": " + idNumber);
            return false;
        }
        return true;
    }
    public void setMonster(Monster monster, int number) {
        monsters[number] = monster;
    }
}