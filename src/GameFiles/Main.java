package GameFiles;

import GUI.MainMenuGUI;
import Players.Player;
import javafx.application.Application;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {

    public static Player player = new Player();

    public static void main(String[] args) {
        writeSysOutToFile();
        Application.launch(MainMenuGUI.class);
    }


    private static void writeSysOutToFile() {
        FileOutputStream file;
        try {
            file = new FileOutputStream("errorLog.txt", true);
            ErrorLogger logger = new ErrorLogger(file, System.out);
            System.setOut(logger);
            System.setErr(logger);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}