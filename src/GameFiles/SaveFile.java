package GameFiles;

import Monsters.Monster;
import Monsters.MonsterList;
import Players.Player;

import java.io.*;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class SaveFile {

    public boolean createSaveFile(String name, String gender) {
        createSavesDirectory();
        int randomNum = ThreadLocalRandom.current().nextInt(0, 65536);
        String playerId = String.format("%05d", randomNum);

        try{
            File file = new File(System.getProperty("user.home") + "/Javamon Saves/" + name + ".txt");
            if(file.exists()) {
                return false;
            } else {
                file.createNewFile();

                PrintWriter writer = new PrintWriter(System.getProperty("user.home") +
                        "/Javamon Saves/" + name + ".txt", "UTF-8");
                writer.println("Name: " + name);
                writer.println("Gender: " + gender);
                writer.println("PlayerId: " + playerId);
                writer.println("Monsters: ");
                writer.println("");
                for(int i = 0; i < 6; i++) {
                    writer.println("MonsterName: ");
                    writer.println("GivenName: ");
                    writer.println("Gender: ");
                    writer.println("AbilityType: primary");
                    writer.println("Nature: ");
                    writer.println("Item: ");
                    writer.println("GainedExp: ");
                    writer.println("IndividualValues: ");
                    writer.println("EffortValues: ");
                    writer.println("Moves: ");
                    writer.println("PpUps: ");
                    writer.println("IdNumber: ");
                    writer.println("OriginalTrainer: ");
                    writer.println("Friendship: ");
                    writer.println("HiddenPower: ");
                    writer.println("LevelMet: ");
                    writer.println("LocationMet: ");
                    writer.println("RemainingHp: ");
                    writer.println("Status: ");
                    writer.println("");
                }
                writer.println("end");
                writer.close();
            }

        }catch (IOException e) {
            System.out.print(e.getMessage());
            System.out.println("Error on creating new save file: " + name + ".txt in " +
                    System.getProperty("user.home") + "/Javamon Saves");
            return false;
        }
        return true;
    }

    public void createSavesDirectory() {
        File file = new File(System.getProperty("user.home") + "/Javamon Saves");
        if(!file.exists()) {
            if(file.mkdir()) {
                System.out.println("Saves directory created in " + System.getProperty("user.home"));
            }
        }
    }

    public Player loadSaveFile(File saveFile) {
        Player player = new Player();
        Monster monster;
        Scanner input = null;
        int monsterCounter = 0;

        if(saveFile != null) {
            try {
                input = new Scanner(saveFile);

            }catch (FileNotFoundException e) {
                System.out.println("No save file found");
            }
        }

        try {
            if (input != null) {
                String[] playerName = input.nextLine().split(" ");
                String[] playerGender = input.nextLine().split(" ");
                String[] playerIdNumber = input.nextLine().split(" ");

                if(!player.setName(playerName[1]) ||
                        !player.setGender(playerGender[1]) ||
                        !player.setIdNumber(playerIdNumber[1])) {
                    return new Player();
                }
                input.nextLine();
                input.nextLine();

                String[] monsterName = input.nextLine().split(" ");
                while(monsterName.length > 1) {
                    String[] givenName = input.nextLine().split(" ");
                    String[] gender = input.nextLine().split(" ");
                    String[] abilityType = input.nextLine().split(" ");
                    String[] nature = input.nextLine().split(" ");
                    String[] item = input.nextLine().split(" ");
                    String[] gainedExp = input.nextLine().split(" ");
                    String[] iv = input.nextLine().split(" ");
                    String[] ev = input.nextLine().split(" ");
                    String[] move = input.nextLine().split(" ");
                    String[] ppUps = input.nextLine().split(" ");
                    String[] idNumber = input.nextLine().split(" ");
                    String[] originalTrainer = input.nextLine().split(" ");
                    String[] friendship = input.nextLine().split(" ");
                    String[] hiddenPower = input.nextLine().split(" ");
                    String[] levelMet = input.nextLine().split(" ");
                    String[] locationMet = input.nextLine().split(" ");
                    String[] remainingHp = input.nextLine().split(" ");
                    String[] status = input.nextLine().split(" ");

                    char genderConv = gender[1].charAt(0);
                    int itemConv = 0;
                    if(item.length > 1)
                        itemConv = Integer.parseInt(item[1]);
                    int gainedExpConv = Integer.parseInt(gainedExp[1]);
                    int[] ivConv = new int[6];
                    int[] evConv = new int[6];
                    String[] moveConv = new String[4];
                    int[] ppUpsConv = new int[4];
                    int remainingHpConv = Integer.parseInt(remainingHp[1]);
                    int friendshipConv = Integer.parseInt(friendship[1]);
                    int hiddenPowerConv = Integer.parseInt(hiddenPower[1]);
                    int levelMetConv = Integer.parseInt(levelMet[1]);

                    for(int i = 0; i < 6; i++)
                        ivConv[i] = Integer.parseInt(iv[i + 1]);
                    for(int i = 0; i < 6; i++)
                        evConv[i] = Integer.parseInt(ev[i + 1]);
                    System.arraycopy(move, 1, moveConv, 0, move.length - 1);
                    for(int i = 0; i < 4; i++)
                        ppUpsConv[i] = Integer.parseInt(ppUps[i + 1]);
                    if(givenName.length < 2)
                        givenName = new String[]{"", ""};
                    if(status.length < 2)
                        status = new String[]{"", ""};

                    monster = new MonsterList().getMonsterByName(monsterName[1]);
                    if(!monster.setGivenName(givenName[1]) ||
                            !monster.setGender(genderConv) ||
                            !monster.setAbilityType(abilityType[1]) ||
                            !monster.setNature(nature[1]) ||
                            !monster.setItem(itemConv) ||
                            !monster.setGainedExp(gainedExpConv) ||
                            !monster.setIndividualValues(ivConv) ||
                            !monster.setEffortValues(evConv) ||
                            !monster.setMoves(moveConv) ||
                            !monster.setPpUps(ppUpsConv) ||
                            !monster.setIdNumber(idNumber[1]) ||
                            !monster.setOriginalTrainer(originalTrainer[1]) ||
                            !monster.setFriendship(friendshipConv) ||
                            !monster.setHiddenPower(hiddenPowerConv) ||
                            !monster.setLevelMet(levelMetConv) ||
                            !monster.setLocationMet(locationMet[1]) ||
                            !monster.setRemainingHp(remainingHpConv) ||
                            !monster.setStatus(status[1])) {
                        return new Player();
                    }
                    player.setMonster(monster, monsterCounter);
                    monsterCounter++;

                    input.nextLine();
                    monsterName = input.nextLine().split(" ");
                }
            }
        }catch (NoSuchElementException ex) {
            System.out.println("Save file is empty or content may be inaccessible");
            player = new Player();
        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("Error on loading save file.");
            ex.printStackTrace();
            player = new Player();
        } finally {
            if(input != null)
                input.close();
        }
        return player;
    }
}
