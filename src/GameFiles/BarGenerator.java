package GameFiles;

import GUI.BarProperty;
import Monsters.Monster;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class BarGenerator {

    public HBox hpBarBox() {
        HBox hpBarBox = new HBox();
        HBox hpBox = new HBox();
        Text hp = new Text("HP");
        hp.setFill(Color.ORANGE);
        hp.setFont(Font.font("Impact", 10));
        hpBox.setStyle("-fx-background-color: black; -fx-background-radius: 5");
        hpBox.setPadding(new Insets(0, 5, 0, 5));

        hpBox.getChildren().add(hp);
        hpBarBox.getChildren().add(0, hpBox);
        hpBarBox.setAlignment(Pos.CENTER_LEFT);

        return hpBarBox;
    }

    private boolean[] getIfColored(int staticValue, int variableValue, int numberOfPieces) {
        boolean[] bar = new boolean[numberOfPieces];
        final double BAR_SIZE = staticValue / ((numberOfPieces - 1) * 1.0);

        bar[0] = variableValue > 0;

        for(int i = 1; i < numberOfPieces; i++) {
            bar[i] = variableValue >= (BAR_SIZE * i) && BAR_SIZE != 0;
        }
        return bar;
    }

    public BarProperty[] setHpBarColor(Monster monster, BarProperty[] hpBarPropertyList) {
        final int NUMBER_OF_PIECES = hpBarPropertyList.length;
        final int MAX_HP = monster.getHP();
        final int REMAINING_HP = monster.getRemainingHP();
        final double HP_LEFT_PERCENTAGE = (REMAINING_HP / (MAX_HP * 1.0)) * 100.0;

        boolean[] hasColor = new BarGenerator().getIfColored(MAX_HP, REMAINING_HP, NUMBER_OF_PIECES);

        final String FX1 = "-fx-background-color: ";
        final String FX2 = "; -fx-border-color: black";
        final String EMPTY_COLOR = FX1 + "white" + FX2;

        for (int i = 0; i < NUMBER_OF_PIECES; i++) {
            if (HP_LEFT_PERCENTAGE > 50) {
                if (hasColor[i]) {
                    hpBarPropertyList[i].setBarPartProperty(FX1 + "green" + FX2);
                }else {
                    hpBarPropertyList[i].setBarPartProperty(EMPTY_COLOR);
                }
            } else if (HP_LEFT_PERCENTAGE > 20) {
                if (hasColor[i]) {
                    hpBarPropertyList[i].setBarPartProperty(FX1 + "orange" + FX2);
                }else {
                    hpBarPropertyList[i].setBarPartProperty(EMPTY_COLOR);
                }
            } else {
                if (hasColor[i]) {
                    hpBarPropertyList[i].setBarPartProperty(FX1 + "red" + FX2);
                } else {
                    hpBarPropertyList[i].setBarPartProperty(EMPTY_COLOR);
                }
            }
        }
        return hpBarPropertyList;
    }
}