package GameFiles;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ErrorLogger extends PrintStream {
    private final PrintStream outToFile;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("[yyyy.MM.dd HH:mm:ss]");
    private Date now = new Date();
    private String date = dateFormat.format(now) + " ";
    private byte[] exceptionHeader = "Exception in thread".getBytes();

     ErrorLogger(OutputStream main, PrintStream outToFile) {
        super(main);

        this.outToFile = outToFile;
    }

    @Override
    public void close() {
        super.close();
        outToFile.close();
    }

    @Override
    public void flush() {
        super.flush();
        outToFile.flush();
    }

    @Override
    public void write(byte[] buf, int off, int len) {
        byte[] bufHeader = new byte[19];
        System.arraycopy(buf, 0, bufHeader, 0, 19);

        if(Arrays.equals(bufHeader, exceptionHeader)) {
            buf = appendDate(buf);
            len += date.length();
        }
        super.write(buf, off, len);
        outToFile.write(buf, off, len);
    }

    @Override
    public void write(int b) {
        super.write(b);
        outToFile.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        super.write(b);
        outToFile.write(b);
    }

    @Override
    public void println(String string) {
        super.println(date + string);
    }

    @Override
    public void println(int integer) {
        super.println(date + integer);
    }

    private byte[] appendDate(byte[] buf) {
        byte[] dateByte = date.getBytes();
        byte[] appended = new byte[dateByte.length + buf.length];
        System.arraycopy(dateByte, 0, appended, 0, dateByte.length);
        System.arraycopy(buf, 0, appended, dateByte.length, buf.length);

        return appended;
    }
}