package Monsters;

import Abilities.Ability;
import Items.Item;
import Lookups.LevelLookup;
import Lookups.NatureLookup;
import Moves.Move;
import Moves.MoveList;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Collections;

public class Monster {

    private String name = "";
    private String givenName = "";
    private char gender = 'A';
    private String abilityType = "";
    private int entryNumber = 0;
    private String[] type = new String[]{"", ""};
    private Ability ability = new Ability();
    private String nature = "";
    private Item item = new Item();
    private int gainedExp = 0;
    private String expGroup = "slow";
    private int[] baseStats = new int[]{1, 1, 1, 1, 1, 1};
    private int[] individualValues = new int[]{0, 0, 0, 0, 0, 0};
    private int[] effortValues = new int[]{0, 0, 0, 0, 0, 0};
    private Move[] moves = new Move[4];
    private ArrayList<String> moveList = new ArrayList<>();
    private int[] ppUps = new int[]{0, 0, 0, 0};
    private String idNumber = "";
    private String originalTrainer = "";
    private int friendship = 0;
    private int hiddenPower = 0;
    private int levelMet = 0;
    private String locationMet = "";
    private int remainingHP = getHP();
    private String status = "";

    private String frontSpritePath = "/Monsters/Sprites/sprite.png";
    private String backSpritePath = "/Monsters/Sprites/sprite.png";
    private String smallSpritePath = "/Monsters/sprites/sprite_small.png";

    public Monster() {
    }

    public String getName(){
        return name.toUpperCase();
    }
    public String getGivenName() {
        if(givenName.equals(""))
            return getName();
        else
            return givenName;
    }
    public char getGender() {
            return gender;
    }
    public String getAbilityType() {
        return abilityType;
    }
    public int getEntryNumber() {
        return entryNumber;
    }
    public String getType(String whichType) {
        switch (whichType) {
            case "primary":
                return type[0].toUpperCase();
            case "secondary":
                return type[1].toUpperCase();
            default:
                return "Unknown type on " + getGivenName() + ": " + whichType;
        }
    }
    public Ability getAbility() {
        return ability;
    }
    public String getNature() {
        return nature;
    }

    public Item getItem() {
        try {
            return item;
        }catch (NullPointerException ex) {
            System.out.println("Unable to get item on " + getGivenName());
        }
        return new Item();
    }
    public int getGainedExp() {
        return gainedExp;
    }
    public int getLevel() {
        if(getEntryNumber() != 0) {
            return new LevelLookup().levelLookup(expGroup, gainedExp);
        }else
            return 0;
    }
    public int getNextLevelExp() {
        return new LevelLookup().nextLevelLookup(expGroup, gainedExp);
    }

    public int getHP() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        return (((2 * baseStats[0] + individualValues[0] + (effortValues[0] / 4)) * level) / 100) + level + 10;
    }
    public int getAttack() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        double natureModifier = new NatureLookup().getNatureModifier(nature, "attack");
        return (int) (((((2 * baseStats[1] + individualValues[1] +
                (effortValues[1] / 4)) * level) / 100) + 5) * natureModifier);
    }
    public int getDefense() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        double natureModifier = new NatureLookup().getNatureModifier(nature, "defense");
        return (int) (((((2 * baseStats[2] + individualValues[2] +
                (effortValues[2] / 4)) * level) / 100) + 5) * natureModifier);
    }
    public int getSpAtk() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        double natureModifier = new NatureLookup().getNatureModifier(nature, "spAtk");
        return (int) (((((2 * baseStats[3] + individualValues[3] +
                (effortValues[3] / 4)) * level) / 100) + 5) * natureModifier);
    }
    public int getSpDef() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        double natureModifier = new NatureLookup().getNatureModifier(nature, "spDef");
        return (int) (((((2 * baseStats[4] + individualValues[4] +
                (effortValues[4] / 4)) * level) / 100) + 5) * natureModifier);
    }
    public int getSpeed() {
        int level = new LevelLookup().levelLookup(expGroup, gainedExp);
        double natureModifier = new NatureLookup().getNatureModifier(nature, "speed");
        return (int) (((((2 * baseStats[5] + individualValues[5] +
                (effortValues[5] / 4)) * level) / 100) + 5) * natureModifier);
    }
    public int getRemainingHP() {
        return remainingHP;
    }
    public String getStatus() {
        String statuses = "brn frz par psn slp";
        if(statuses.contains(status.toLowerCase())) {
            return status.toLowerCase();
        }else{
            System.out.println("Unable to get status on " + getGivenName() + ": " + status);
            return "";
        }
    }
    public Image getSprite(String facing) {
        switch (facing) {
            case "front":
                return new Image(frontSpritePath);
            case "back":
                return new Image(backSpritePath);
            case "small":
                return new Image(smallSpritePath);
            default:
                System.out.println("Sprite type on " + getGivenName() + " not found: " + facing);
                return getSprite();
        }
    }
    public Image getSprite() {
        return new Image("/Monsters/Sprites/sprite.png");
    }
    public Move getMove(int number) {
        if(number >= 0 && number < 4) {
            try {
                return moves[number];
            }catch (NullPointerException ex) {
                System.out.println("Unable to get move " + number + " on " + getGivenName());
            }
            return new Move();
        }
        else {
            System.out.println("Move number on " + getGivenName() + " out of range: " + number);
            return new Move();
        }
    }
    public ArrayList<String> getMoveList() {
        return moveList;
    }
    public int getPpUp(int number) {
        if(number >= 0 && number < 4)
        return ppUps[number];
        else {
            System.out.println("Move number on " + getGivenName() + " out of range: " + number);
            return 0;
        }
    }
    public String getIdNumber() {
        return idNumber;
    }
    public String getOriginalTrainer() {
        return originalTrainer;
    }
    public int getFriendship() {
        return friendship;
    }
    public int getHiddenPower() {
        return hiddenPower;
    }
    public int getLevelMet() {
        return levelMet;
    }
    public String getLocationMet() {
        return locationMet;
    }

    public void setName(String name) {
        try {
            if(!name.equals("") && name.length() < 11)
                this.name = name.toUpperCase();
            else
                System.out.println("Invalid name on " + getGivenName() + ": " + name);
        }catch (NullPointerException ex) {
            System.out.println("Name cannot be null on: " + getGivenName());
        }
    }
    public boolean setGivenName(String givenName) {
        try {
            if(givenName.length() < 11) {
                this.givenName = givenName;
                return true;
            }else
                System.out.println("Invalid name on " + getGivenName() + ": " + givenName);
        }catch (NullPointerException ex) {
            System.out.println("Name cannot be null on: " + getGivenName());
        }
        return false;
    }
    public boolean setGender(char gender) {
        if(gender == 'm' || gender == 'M')
            this.gender = 'M';
        else if(gender == 'f' || gender == 'F')
            this.gender = 'F';
        else if(gender == 'a' || gender == 'A')
            this.gender = 'A';
        else {
            System.out.println("Invalid gender on " + getGivenName() + ": " + gender);
            return false;
        }
        return true;
    }
    public boolean setAbilityType(String abilityType) {
        if(abilityType.equals("primary") || abilityType.equals("hidden"))
            this.abilityType = abilityType;
        else {
            System.out.println("Invalid ability type on " + getGivenName() + ": " + abilityType);
            return false;
        }
        return true;
    }
    public void setEntryNumber(int entryNumber) {
        if(entryNumber > 0 && entryNumber < 1000)
            this.entryNumber = entryNumber;
        else
            System.out.println("Invalid entry number on " + getGivenName() + ": " + entryNumber);
    }
    public void setType(String primary, String secondary) {
        String types = "normal fire fighting water flying grass poison electric ground " +
                "psychic rock ice bug dragon ghost dark steel fairy ???";
        if(types.contains(primary.toLowerCase()))
            type[0] = primary.toLowerCase();
        else
            System.out.println("Error on setting primary type on " + getGivenName() + ": " + primary);
        if(types.contains(secondary.toLowerCase()))
            type[1] = secondary.toLowerCase();
        else
            System.out.println("Error on setting secondary type on " + getGivenName() + ": " + secondary);
    }
    public void setAbility(Ability ability) {
        this.ability = ability;
    }
    public boolean setNature(String nature) {
        String natures = "hardy lonely brave adamant naughty bold docile relaxed impish" +
                "lax timid hasty serious jolly naive modest mild quiet bashful rash calm gentle" +
                "sassy careful quirky";
        if(natures.contains(nature.toLowerCase()))
            this.nature = nature.toLowerCase();
        else {
            System.out.println("Error on setting nature on " + getGivenName() + ": " + nature);
            return false;
        }
        return true;
    }
    //not implemented
    public boolean setItem(int item) {
        try {
            this.item = new Item();
        }catch (NullPointerException ex) {
            System.out.println("Error on setting item on " + getGivenName());
            this.item = new Item();
            return false;
        }
        return true;
    }
    public boolean setGainedExp(int gainedExp) {
        if(gainedExp >= 0 && gainedExp <= new LevelLookup().getMaxExp(expGroup))
            this.gainedExp = gainedExp;
        else {
            System.out.println("Experience points out of range on " + getGivenName() + ": " + gainedExp);
            this.gainedExp = 0;
            return false;
        }
        return true;
    }
    public void setBaseStats(int hp, int attack, int defense, int spAtk, int spDef, int speed) {
        if(hp > 0 && attack > 0 && defense > 0 && spAtk > 0 && spDef > 0 && speed > 0) {
            baseStats[0] = hp;
            baseStats[1] = attack;
            baseStats[2] = defense;
            baseStats[3] = spAtk;
            baseStats[4] = spDef;
            baseStats[5] = speed;
        }
        else
            System.out.println("One or more base stat out of range on " + getGivenName() + ": " +
                    hp +  " " + attack +  " " + defense +  " " + spAtk +  " " + spDef +  " " + speed);
    }
    public boolean setIndividualValues(int[] iv) {
        try{
            for(int i = 0; i < 6; i++) {
                if(iv[i] >= 0 && iv[i] <= 31) {
                    individualValues[i] = iv[i];
                }else{
                    System.out.println("Invalid individual value " + i + " on " + getGivenName() + ": " + iv[i]);
                    return false;
                }
            }
        }catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid number of iv's on " + getGivenName() + ": " + iv.length);
            return false;
        }
        return true;
    }
    public boolean setEffortValues(int[] ev) {
        try{
            for(int i = 0; i < 6; i++) {
                if(ev[i] >= 0 && ev[i] <= 252) {
                    effortValues[i] = ev[i];
                }else{
                    System.out.println("Invalid individual value " + i + " on " + getGivenName() + ": " + ev[i]);
                    return false;
                }
            }
        }catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Invalid number of iv's on " + getGivenName() + ": " + ev.length);
            return false;
        }
        return true;
    }
    public void setMoveList(String[] moves) {
        Collections.addAll(moveList, moves);
    }

    public boolean setMoves(String[] moves) {
        for(int i = 0; i < 4; i++) {
            try {
                this.moves[i] = new MoveList().getMoveByName(moves[i], getPpUp(i));
            }catch (NullPointerException ex) {
                System.out.println("Unable to set move " + i + " on " + getGivenName());
                return false;
            }
        }
        return true;
    }
    public boolean setPpUps(int[] ppUps) {
        for(int i = 0; i < 4; i++) {
            if(ppUps[i] >= 0 && ppUps[i] <= 4) {
                this.ppUps[i] = ppUps[i];
            }else {
                System.out.println("Invalid ppUp value on " + getGivenName() + ": " + ppUps[i]);
                return false;
            }
        }
        return true;
    }
    public boolean setIdNumber(String idNumber) {
        if(Integer.parseInt(idNumber) > 0 && Integer.parseInt(idNumber) < 65536) {
            this.idNumber = idNumber;
        }else {
            System.out.println("Id number out of range on " + getGivenName() + ": " + idNumber);
            return false;
        }
        return true;
    }
    public boolean setOriginalTrainer(String originalTrainer) {
        if(!originalTrainer.equals("") && originalTrainer.length() < 16) {
            this.originalTrainer = originalTrainer;
        }else {
            System.out.println("Invalid original trainer name on " + getGivenName() + ": " + originalTrainer);
            return false;
        }
        return true;
    }
    public boolean setFriendship(int friendship) {
        if(friendship >= 0 && friendship < 256) {
            this.friendship = friendship;
        }else {
            System.out.println("Friendship out of range on " + getGivenName() + ": " + friendship);
            return false;
        }
        return true;
    }
    public boolean setHiddenPower(int hiddenPower) {
        if(hiddenPower >= 30 && hiddenPower <= 70) {
            this.hiddenPower = hiddenPower;
        }else {
            System.out.println("Hidden power out of range on " + getGivenName() + ": " + hiddenPower);
            return false;
        }
        return true;
    }
    public boolean setLevelMet(int levelMet) {
        if(levelMet > 0 && levelMet <= 100) {
            this.levelMet = levelMet;
        }else {
            System.out.println("Level met out of range on " + getGivenName() + ": " + levelMet);
            return false;
        }
        return true;
    }
    public boolean setLocationMet(String locationMet) {
        this.locationMet = locationMet;
        return true;
    }
    public boolean setRemainingHp(int remainingHP) {
        if(remainingHP >= 0 && remainingHP <= getHP())
            this.remainingHP = remainingHP;
        else {
            System.out.println("Remaining hp out of range on " + getGivenName() + ": " + remainingHP);
            return false;
        }
        return true;
    }
    public boolean setStatus(String status) {
        String statuses = "brn frz par psn slp";
        if(statuses.contains(status.toLowerCase())) {
            this.status = status.toLowerCase();
            return true;
        }else{
            System.out.println("Unable to set status on " + getGivenName() + ": " + status);
            this.status = "";
            return false;
        }
    }
    public void setSpritePath(String facing, String path) {
        facing = facing.toLowerCase();

        if(facing.equals("front"))
            this.frontSpritePath = path;
        else if(facing.equals("back"))
            this.backSpritePath = path;
        else if(facing.equals("small"))
            this.smallSpritePath = path;
        else
            System.out.println("Sprite type on " + getGivenName() + " not found: " + facing);
    }
}