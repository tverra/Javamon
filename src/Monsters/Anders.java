package Monsters;

import Abilities.Pressure;

public class Anders extends Monster {
    String[] moveList = new String[]{
            "bodyslam", "firepunch", "flamecharge", "flareblitz", "will-o-wisp"
    };

    public Anders() {
        super.setName("anders");
        super.setEntryNumber(1);
        super.setType("fire", "");
        super.setAbility(new Pressure());
        super.setBaseStats(100, 70, 80, 60, 80, 110);
        super.setMoveList(moveList);
        super.setSpritePath("front", "/Monsters/Sprites/anders_front.png");
        super.setSpritePath("back", "/Monsters/Sprites/anders_back.png");
        super.setSpritePath("small", "/Monsters/Sprites/anders_small.png");
    }

    public Anders(String givenName, char gender, String abilityType, String nature, int item,
                  int gainedExp, int[] iv, int[] ev, String[] move, int[] ppUps, String idNumber,
                  String originalTrainer, int friendship, int hiddenPower, int levelMet,
                  String locationMet, int remainingHp, String status) {
        super.setName("anders");
        super.setGivenName(givenName);
        super.setGender(gender);
        super.setAbilityType(abilityType);
        super.setEntryNumber(1);
        super.setType("fire", "");
        super.setAbility(new Pressure());
        super.setNature(nature);
        super.setItem(item);
        super.setGainedExp(gainedExp);
        super.setBaseStats(100, 70, 80, 60, 80, 110);
        super.setIndividualValues(iv);
        super.setEffortValues(ev);
        super.setMoves(move);
        super.setMoveList(moveList);
        super.setPpUps(ppUps);
        super.setIdNumber(idNumber);
        super.setOriginalTrainer(originalTrainer);
        super.setFriendship(friendship);
        super.setHiddenPower(hiddenPower);
        super.setLevelMet(levelMet);
        super.setLocationMet(locationMet);
        super.setRemainingHp(remainingHp);
        super.setStatus(status);
        super.setSpritePath("front", "/Monsters/Sprites/anders_front.png");
        super.setSpritePath("back", "/Monsters/Sprites/anders_back.png");
        super.setSpritePath("small", "/Monsters/Sprites/anders_small.png");
    }
}
