package Monsters;

public class MonsterList {
    public Monster getMonsterByName(String name) {

        name = name.toLowerCase();

        if(name.contains("anders"))
            return new Anders();
        else {
            System.out.println("Unable to load monster by name: " + name);
            return new Monster();
        }
    }
}
