package GUI;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class Summary {
    public HBox clickForMonsterSummary(HBox monsterBox) {
        monsterBox.setOnMouseEntered(e -> {
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(monsterBox.minHeightProperty(), 50)),
                    new KeyFrame(Duration.ZERO, new KeyValue(monsterBox.minWidthProperty(), 250)),
                    new KeyFrame(Duration.millis(100), new KeyValue(monsterBox.minHeightProperty(), 55)),
                    new KeyFrame(Duration.millis(100), new KeyValue(monsterBox.minWidthProperty(), 255))
            );
            timeline.play();
        });
        monsterBox.setOnMouseExited(e -> {
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(monsterBox.minHeightProperty(), 55)),
                    new KeyFrame(Duration.ZERO, new KeyValue(monsterBox.minWidthProperty(), 255)),
                    new KeyFrame(Duration.millis(100), new KeyValue(monsterBox.minHeightProperty(), 50)),
                    new KeyFrame(Duration.millis(100), new KeyValue(monsterBox.minWidthProperty(), 250))
            );
            timeline.play();
        });

        return monsterBox;
    }

    public VBox clickForPlayerSummary(VBox playerBox) {
        playerBox.setOnMouseEntered(e -> {
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(playerBox.minHeightProperty(), 80)),
                    new KeyFrame(Duration.ZERO, new KeyValue(playerBox.minWidthProperty(), 280)),
                    new KeyFrame(Duration.millis(100), new KeyValue(playerBox.minHeightProperty(), 85)),
                    new KeyFrame(Duration.millis(100), new KeyValue(playerBox.minWidthProperty(), 285))
            );
            timeline.play();
        });
        playerBox.setOnMouseExited(e -> {
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().addAll(
                    new KeyFrame(Duration.ZERO, new KeyValue(playerBox.minHeightProperty(), 85)),
                    new KeyFrame(Duration.ZERO, new KeyValue(playerBox.minWidthProperty(), 285)),
                    new KeyFrame(Duration.millis(100), new KeyValue(playerBox.minHeightProperty(), 80)),
                    new KeyFrame(Duration.millis(100), new KeyValue(playerBox.minWidthProperty(), 280))
            );
            timeline.play();
        });

        return playerBox;
    }
}
