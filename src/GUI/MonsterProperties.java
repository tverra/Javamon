package GUI;

import javafx.beans.property.*;
import javafx.scene.image.Image;

public class MonsterProperties {
    private ObjectProperty<Image> monsterSprite = new SimpleObjectProperty<>();
    private StringProperty monsterName = new SimpleStringProperty();
    private StringProperty monsterLevel = new SimpleStringProperty();
    private StringProperty monsterGender = new SimpleStringProperty();
    private StringProperty monsterBox = new SimpleStringProperty("-fx-opacity: 0");
    private StringProperty monsterMaxHp = new SimpleStringProperty();
    private StringProperty monsterRemainingHp = new SimpleStringProperty();
    private StringProperty monsterStatusProperty = new SimpleStringProperty();
    private StringProperty monsterStatusColorProperty = new SimpleStringProperty("");

    public ObjectProperty<Image> monsterSpriteProperty() {
        return monsterSprite;
    }
    public StringProperty monsterNameProperty() {
        return monsterName;
    }
    public StringProperty monsterLevelProperty() {
        return monsterLevel;
    }
    public StringProperty monsterGenderProperty() {
        return monsterGender;
    }
    public StringProperty monsterBoxProperty() {
        return monsterBox;
    }
    public StringProperty monsterMaxHpProperty() {
        return monsterMaxHp;
    }
    public StringProperty monsterRemainingHpProperty() {
        return monsterRemainingHp;
    }
    public StringProperty monsterStatusProperty() {
        return monsterStatusProperty;
    }
    public StringProperty monsterStatusColorProperty() {
        return monsterStatusColorProperty;
    }

    public Image getMonsterSpriteProperty() {
        return monsterSprite.get();
    }
    public String getMonsterNameProperty() {
        return monsterName.get();
    }
    public String getMonsterLevelProperty() {
        return monsterLevel.get();
    }
    public String getMonsterGenderProperty() {
        return monsterGender.get();
    }
    public String getMonsterBoxProperty() {
        return monsterBox.get();
    }
    public String getMonsterMaxHpProperty() {
        return monsterMaxHp.get();
    }
    public String getMonsterRemainingHpProperty() {
        return monsterRemainingHp.get();
    }
    public String getMonsterStatusProperty() {
        return monsterStatusProperty.get();
    }
    public String getMonsterStatusColorProperty() {
        return monsterStatusColorProperty.get();
    }

    public void setMonsterSprite(Image monsterSprite) {
        this.monsterSprite.set(monsterSprite);
    }
    public void setMonsterName(String monsterName) {
        this.monsterName.set(monsterName);
    }
    public void setMonsterLevel(int monsterLevel) {
        this.monsterLevel.set(Integer.toString(monsterLevel));
    }
    public void setMonsterGender(char monsterGender) {
        if(monsterGender == 'M') {
            this.monsterGender.set(Character.toString((char)0x2640));
        }else if(monsterGender == 'F') {
            this.monsterGender.set(Character.toString((char)0x2642));
        }else if(monsterGender == 'A') {
            this.monsterGender.set(Character.toString((char)0x26B2));
        }else {
            System.out.println("Unable to set gender property on " + getMonsterNameProperty() + ": " +
            monsterGender);
            this.monsterGender.set(Character.toString((char)0x26B2));
        }
    }
    public void setMonsterBox(String monsterBox) {
        this.monsterBox.set(monsterBox);
    }
    public void setMonsterMaxHp(int monsterMaxHp) {
        this.monsterMaxHp.set(Integer.toString(monsterMaxHp));
    }
    public void setMonsterRemainingHp(int monsterRemainingHp) {
        this.monsterRemainingHp.set(Integer.toString(monsterRemainingHp));
    }
    public void setMonsterStatus(String status) {
        this.monsterStatusProperty.set(status);
    }
    public void setMonsterStatusColor(String status) {
        if(status.toLowerCase().equals("brn"))
            this.monsterStatusColorProperty.set("-fx-background-color: red; -fx-background-radius: 5");
        else if(status.toLowerCase().equals("frz"))
            this.monsterStatusColorProperty.set("-fx-background-color: lightblue; -fx-background-radius: 5");
        else if(status.toLowerCase().equals("par"))
            this.monsterStatusColorProperty.set("-fx-background-color: orange; -fx-background-radius: 5");
        else if(status.toLowerCase().equals("psn"))
            this.monsterStatusColorProperty.set("-fx-background-color: purple; -fx-background-radius: 5");
        else if(status.toLowerCase().equals("slp"))
            this.monsterStatusColorProperty.set("-fx-background-color: B9A796; -fx-background-radius: 5");
        else
            this.monsterStatusColorProperty.set("");
    }
}