package GUI;

import javafx.beans.property.*;
import javafx.scene.image.Image;

public class PlayerProperties {
    private StringProperty playerName = new SimpleStringProperty();
    private ObjectProperty<Image> playerSprite = new SimpleObjectProperty<>();
    private StringProperty playerBox = new SimpleStringProperty();

    public StringProperty playerNameProperty() {
        return playerName;
    }
    public ObjectProperty<Image> playerSpriteProperty() {
        return playerSprite;
    }
    public StringProperty playerBoxProperty() {
        return playerBox;
    }

    public String getPlayerNameProperty() {
        return playerName.get();
    }
    public Image getPlayerSpriteProperty() {
        return playerSprite.get();
    }
    public String getPlayerBoxProperty() {
        return playerBox.get();
    }

    public void setPlayerName(String playerName) {
        this.playerName.set(playerName);
    }
    public void setPlayerSprite(char playerGender) {
        if(playerGender == 'M') {
            this.playerSprite.set(new Image("/GameFiles/Sprites/player1m_small.png"));
        }else if(playerGender == 'F') {
            this.playerSprite.set(new Image("/GameFiles/Sprites/player1f_small.png"));
        }else if(playerGender == 'A') {
            this.playerSprite.set(new Image("/GameFiles/Sprites/player1a_small.png"));
        }else {
            this.playerSprite.set(new Image("/GameFiles/Sprites/player1a_small.png"));
            System.out.println("Unable to set player sprite property on " + getPlayerNameProperty() +
                    ": invalid gender: " + playerGender);
        }
    }
    public void setPlayerBox(String playerBox) {
        this.playerBox.set(playerBox);
    }
}
