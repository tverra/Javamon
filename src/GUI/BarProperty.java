package GUI;

import javafx.beans.property.*;

public class BarProperty {
    private StringProperty barPart = new SimpleStringProperty();

    public StringProperty barPartProperty() {
        return barPart;
    }

    public String getBarPart() {
        return barPart.get();
    }

    public void setBarPartProperty(String barColor) {
        this.barPart.set(barColor);
    }
}