package GUI;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

class StartMenu {
    Text getTitle(String text){
        Text title = new Text(text);
        title.setFont(Font.font("Tempus Sans ITC", 140));
        title.setStyle("-fx-font-weight: bold");
        StackPane.setAlignment(title, Pos.TOP_CENTER);
        return title;
    }

    Text getExitButton() {
        Text exitButton = new Text("Exit");
        exitButton.setFont(Font.font("Tempus Sans ITC", 60));
        exitButton.setOnMouseEntered(e -> exitButton.setStyle("-fx-font-size: 70"));
        exitButton.setOnMouseExited(e -> exitButton.setStyle("-fx-font-size: 60"));
        StackPane.setAlignment(exitButton, Pos.BOTTOM_CENTER);
        StackPane.setMargin(exitButton, new Insets(0, 0, 50, 0));
        exitButton.setOnMouseClicked(e -> Platform.exit());
        return exitButton;
    }

    public Text getCenterText(String text, int size) {
        Text centerText = new Text(text);
        centerText.setFont(Font.font("Tempus Sans ITC", size));
        StackPane.setAlignment(centerText, Pos.CENTER);
        return centerText;
    }
}
