package GUI;

import GameFiles.BarGenerator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


class DisplayBox {

    VBox playerPreview(Text playerName, ImageView playerSprite) {
        VBox playerBox = new VBox();
        VBox nameBox = new VBox();

        playerName.setFont(Font.font("Tempus Sans ITC", 20));
        playerBox.setMaxWidth(280);
        playerBox.setMinHeight(80);
        playerBox.setPadding(new Insets(0, 0, 0, 5));
        nameBox.getChildren().add(playerName);
        nameBox.setAlignment(Pos.CENTER);
        playerBox.getChildren().addAll(nameBox, playerSprite);

        return playerBox;
    }

    HBox monsterBox(ImageView sprite, Text name, Text level, Text gender, VBox[] hpBarList,
                    Text maxHp, Text remainingHp, Text status, VBox statusColor) {
        HBox monsterBox = new HBox();
        VBox spriteBox = new VBox();
        VBox monsterNameBox = new VBox();
        HBox levelAndGenderBox = new HBox();
        HBox levelBox = new HBox();
        HBox genderBox = new HBox();
        HBox hpBox = new HBox();
        HBox hpBarBox = new BarGenerator().hpBarBox();

        monsterNameBox.setAlignment(Pos.CENTER_LEFT);
        monsterBox.setStyle("-fx-border-color: black; -fx-border-radius: 5");
        monsterBox.setMaxWidth(250);
        monsterBox.setMinHeight(50);
        monsterBox.setAlignment(Pos.CENTER_LEFT);
        spriteBox.setMinWidth(50);
        spriteBox.setAlignment(Pos.CENTER_LEFT);
        levelBox.setMinWidth(40);
        genderBox.setMinWidth(20);
        status.setStyle("-fx-fill: white; -fx-font-weight: bold; -fx-font-size: 10");
        statusColor.setPadding(new Insets(0, 0, 2, 0));
        statusColor.setMinWidth(30);
        statusColor.setMaxHeight(1);
        statusColor.setAlignment(Pos.CENTER);
        statusColor.getChildren().add(status);
        hpBox.setMinWidth(99);
        hpBox.setAlignment(Pos.CENTER_RIGHT);

        for(int i = 0; i < 21; i++) {
            hpBarList[i].setMinWidth(8);
            hpBarList[i].setMaxHeight(7);
            hpBarBox.getChildren().add(hpBarList[i]);
        }
        hpBox.getChildren().addAll(remainingHp, new Text(" / "), maxHp);
        genderBox.getChildren().add(gender);
        levelBox.getChildren().addAll(new Text("Lv"), level);
        levelAndGenderBox.getChildren().addAll(levelBox, genderBox, statusColor, hpBox);
        monsterNameBox.getChildren().addAll(name, levelAndGenderBox, hpBarBox);
        spriteBox.getChildren().add(sprite);
        monsterBox.getChildren().addAll(spriteBox, monsterNameBox);

        return monsterBox;
    }
}
