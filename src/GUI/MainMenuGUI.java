package GUI;

import GameFiles.BarGenerator;
import GameFiles.SaveFile;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

import static GameFiles.Main.player;

public class MainMenuGUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private int numberOfStepsToRoot = 2;

    private StackPane startMenuPane = new StackPane();
    private StackPane saveMenuPane = new StackPane();
    private StackPane newSaveMenuPane = new StackPane();
    private static StartMenu startMenu = new StartMenu();
    private static File saveFile;

    private PlayerProperties playerProperties = new PlayerProperties();
    private MonsterProperties[] monsterPropertiesList = new MonsterProperties[6];
    private BarProperty[][] monsterHpBarPropertyList = new BarProperty[6][21];

    @Override
    public void start(Stage primaryStage) {
        final int WINDOW_HEIGHT = 800;
        final int WINDOW_WIDTH = 1200;

        startMenuPane.setMinSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        startMenuPane.setStyle("-fx-padding: 10px; -fx-background-color: aliceblue");
        startMenuPane.getChildren().addAll(
                startMenu.getTitle("Javamon"),
                savesButton(),
                startMenu.getExitButton()
        );

        saveMenuPane.setMinSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        saveMenuPane.setStyle("-fx-padding: 10px; -fx-background-color: aliceblue");
        saveMenuPane.getChildren().addAll(
                startMenu.getTitle("Saves"),
                saveFilePreview(),
                openSaveFile(),
                newSaveButton(),
                backButton()
        );

        newSaveMenuPane.setMinSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        newSaveMenuPane.setStyle("-fx-padding: 10px; -fx-background-color: aliceblue");

        primaryStage.setMinHeight(WINDOW_HEIGHT);
        primaryStage.setMinWidth(WINDOW_WIDTH);
        primaryStage.setTitle("Javamon");
        primaryStage.setScene(new Scene(startMenuPane, WINDOW_WIDTH, WINDOW_HEIGHT));
        primaryStage.sizeToScene();
        primaryStage.show();
    }



    private void setButtonStyle(Text textButton, int fontSize){
        textButton.setFont(Font.font("Tempus Sans ITC", fontSize));
        textButton.setOnMouseEntered(e -> textButton.setStyle("-fx-font-size:" + (fontSize + 5)));
        textButton.setOnMouseExited(e -> textButton.setStyle("-fx-font-size:" + fontSize));
    }

    private Text savesButton() {
        Text savesButton = new Text("Load Save File");
        setButtonStyle(savesButton, 60);
        StackPane.setAlignment(savesButton, Pos.CENTER);
        StackPane.setMargin(savesButton, new Insets(0, 0, 0, 0));
        savesButton.setOnMouseClicked(e-> {
            startMenuPane.getChildren().add(saveMenuPane);
            numberOfStepsToRoot++;
        });
        return savesButton;
    }

    private Node newSaveButton() {
        Text newSaveButton = new Text("Create New Save File");
        setButtonStyle(newSaveButton, 60);
        StackPane.setAlignment(newSaveButton, Pos.CENTER);
        StackPane.setMargin(newSaveButton, new Insets(100, 0, 0, 0));
        newSaveButton.setOnMouseClicked(e-> {

            newSaveMenuPane.getChildren().clear();
            newSaveMenuPane.getChildren().addAll(
                    startMenu.getTitle("New SaveFile"),
                    newSaveForm(),
                    backButton()
            );
            startMenuPane.getChildren().add(newSaveMenuPane);
            numberOfStepsToRoot++;
        });
        return newSaveButton;
    }

    private VBox saveFilePreview() {
        Text playerName = new Text();
        ImageView playerSprite = new ImageView();
        ImageView[] monsterSpriteList = new ImageView[6];
        Text[] monsterNameList = new Text[6];
        Text[] monsterLevelList = new Text[6];
        Text[] monsterGenderList = new Text[6];
        Text[] monsterMaxHpList = new Text[6];
        Text[] monsterRemainingHpList = new Text[6];
        Text[] monsterStatusList = new Text[6];
        VBox[] monsterStatusColorList = new VBox[6];
        VBox[][] monsterHpBarList = new VBox[6][21];
        VBox[] monsterBoxOutlineList = new VBox[6];
        VBox vBox = new VBox();
        VBox playerBox = new DisplayBox().playerPreview(playerName, playerSprite);
        VBox playerBoxOutline = new VBox();
        playerBoxOutline.setStyle("-fx-border-color: black; -fx-border-radius: 5");
        playerBoxOutline.setMaxWidth(282);

        playerSprite.imageProperty().bind(playerProperties.playerSpriteProperty());
        playerName.textProperty().bind(playerProperties.playerNameProperty());
        playerBox.styleProperty().bind(playerProperties.playerBoxProperty());
        playerBox = new Summary().clickForPlayerSummary(playerBox);
        playerBoxOutline.getChildren().add(playerBox);
        vBox.getChildren().add(playerBoxOutline);

        for(int i = 0; i < 6; i++) {
            monsterSpriteList[i] = new ImageView();
            monsterNameList[i] = new Text();
            monsterLevelList[i] = new Text();
            monsterGenderList[i] = new Text();
            monsterMaxHpList[i] = new Text();
            monsterRemainingHpList[i] = new Text();
            monsterStatusList[i] = new Text();
            monsterStatusColorList[i] = new VBox();

            monsterBoxOutlineList[i] = new VBox();
            monsterBoxOutlineList[i].setStyle("-fx-border-color: black; -fx-border-radius: 5");
            monsterBoxOutlineList[i].setMaxWidth(252);
            monsterPropertiesList[i] = new MonsterProperties();
            for(int j = 0; j < 21; j++) {
                monsterHpBarPropertyList[i][j] = new BarProperty();
                monsterHpBarList[i][j] = new VBox();
                monsterHpBarList[i][j].styleProperty().bind(monsterHpBarPropertyList[i][j].barPartProperty());
            }
            monsterSpriteList[i].imageProperty().bind(monsterPropertiesList[i].monsterSpriteProperty());
            monsterNameList[i].textProperty().bind(monsterPropertiesList[i].monsterNameProperty());
            monsterLevelList[i].textProperty().bind(monsterPropertiesList[i].monsterLevelProperty());
            monsterGenderList[i].textProperty().bind(monsterPropertiesList[i].monsterGenderProperty());
            monsterMaxHpList[i].textProperty().bind(monsterPropertiesList[i].monsterMaxHpProperty());
            monsterRemainingHpList[i].textProperty().bind(monsterPropertiesList[i].monsterRemainingHpProperty());
            monsterStatusList[i].textProperty().bind(monsterPropertiesList[i].monsterStatusProperty());
            monsterStatusColorList[i].styleProperty().bind(monsterPropertiesList[i].monsterStatusColorProperty());

            HBox monsterBox = new DisplayBox().monsterBox(monsterSpriteList[i], monsterNameList[i],
                    monsterLevelList[i], monsterGenderList[i], monsterHpBarList[i], monsterMaxHpList[i],
                    monsterRemainingHpList[i], monsterStatusList[i], monsterStatusColorList[i]);
            monsterBox.styleProperty().bind(monsterPropertiesList[i].monsterBoxProperty());
            monsterBox = new Summary().clickForMonsterSummary(monsterBox);

            monsterBoxOutlineList[i].getChildren().add(monsterBox);
            vBox.getChildren().add(monsterBoxOutlineList[i]);
        }
        vBox.setPadding(new Insets(0, 10, 0, 10));
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER_LEFT);

        return vBox;
    }

    private void setSaveFileProperties() {
        playerProperties.setPlayerName(player.getName());
        playerProperties.setPlayerSprite(player.getGender());
        if(player.getName().equals("")) {
            playerProperties.setPlayerBox("-fx-opacity: 0");
        }else {
            playerProperties.setPlayerBox("-fx-opacity: 1");
        }

        for(int i = 0; i < player.getMonsters().length; i++) {
            monsterPropertiesList[i].setMonsterSprite(player.getMonsters()[i].getSprite("small"));
            monsterPropertiesList[i].setMonsterName(player.getMonsters()[i].getGivenName());
            monsterPropertiesList[i].setMonsterLevel(player.getMonsters()[i].getLevel());
            monsterPropertiesList[i].setMonsterGender(player.getMonsters()[i].getGender());
            monsterHpBarPropertyList[i] = new BarGenerator().setHpBarColor(player.getMonsters()[i], monsterHpBarPropertyList[i]);
            monsterPropertiesList[i].setMonsterStatus(player.getMonsters()[i].getStatus());
            monsterPropertiesList[i].setMonsterStatusColor(player.getMonsters()[i].getStatus());
            monsterPropertiesList[i].setMonsterStatusColor(player.getMonsters()[i].getStatus());
            monsterPropertiesList[i].setMonsterMaxHp(player.getMonsters()[i].getHP());
            monsterPropertiesList[i].setMonsterRemainingHp(player.getMonsters()[i].getRemainingHP());

            if(player.getMonsters()[i].getEntryNumber() == 0) {
                monsterPropertiesList[i].setMonsterBox("-fx-opacity: 0");
            }else {
                monsterPropertiesList[i].setMonsterBox("-fx-opacity: 1");
            }
        }
    }

    private Text backButton() {
        Text backButton = new Text("Back");
        setButtonStyle(backButton, 60);
        StackPane.setAlignment(backButton, Pos.BOTTOM_CENTER);
        StackPane.setMargin(backButton, new Insets(0, 0, 50, 0));
        backButton.setOnMouseClicked(e -> {
            startMenuPane.getChildren().remove(numberOfStepsToRoot);
            numberOfStepsToRoot--;
        });
        return backButton;
    }

    private Text openSaveFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Javamon Saves"));
        Text openFile = new Text("Open Save File");
        setButtonStyle(openFile, 60);
        StackPane.setAlignment(openFile, Pos.CENTER);
        StackPane.setMargin(openFile, new Insets(0, 0, 100, 0));

        openFile.setOnMouseClicked(event -> {
            new SaveFile().createSavesDirectory();
            File file = fileChooser.showOpenDialog(new Stage());
            if (file != null) {
                saveFile = file;
                player = new SaveFile().loadSaveFile(saveFile);
                setSaveFileProperties();
            }
        });
        return openFile;
    }

    private void openSaveFile(String fileName) {
        saveFile = new File(System.getProperty("user.home") + "\\Javamon Saves\\" + fileName);
        player = new SaveFile().loadSaveFile(saveFile);
        setSaveFileProperties();
    }

    private HBox newSaveForm() {
        Label emptyLabel = new Label("");
        Text successText = new StartMenu().getCenterText("New save file created!", 40);
        StackPane.setAlignment(successText, Pos.CENTER);

        VBox inputBox = new VBox();
        TextField nameTextField = new TextField();
        nameTextField.setFont(Font.font("Tempus Sans ITC", 30));
        HBox.setHgrow(nameTextField, Priority.ALWAYS);
        VBox.setMargin(nameTextField, new Insets(0, 0, 20, 0));
        ChoiceBox genderChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(
                "Male", "Female", "Apache"));
        genderChoiceBox.setStyle("-fx-font: 30px \"Tempus Sans ITC\";");
        genderChoiceBox.setMinWidth(180);

        Button submitButton = new Button("Submit");
        submitButton.setStyle("-fx-font: 30px \"Tempus Sans ITC\";");
        VBox.setMargin(submitButton, new Insets(20, 0, 20, 0));

        submitButton.setOnAction(e -> {
            if(!nameTextField.getText().equals("") && nameTextField.getText().length() <= 15 &&
                    !genderChoiceBox.getSelectionModel().isEmpty()) {
                String name = nameTextField.getText();
                String gender = genderChoiceBox.getValue().toString().toLowerCase();

                if(new SaveFile().createSaveFile(name, gender)) {
                    player.setName(name);
                    player.setGender(gender);
                    newSaveMenuPane.getChildren().remove(1, 2);
                    newSaveMenuPane.getChildren().add(successText);
                    openSaveFile(name + ".txt");
                }else {
                    try {
                        inputBox.getChildren().remove(3);
                    }catch (IndexOutOfBoundsException ex){
                        ex.printStackTrace();
                    }
                    inputBox.getChildren().add(3, new StartMenu().getCenterText(
                            "Try using a different name", 20));
                }
            } else {
                try {
                    inputBox.getChildren().remove(3);
                }catch (IndexOutOfBoundsException ex){
                    ex.printStackTrace();
                }

                if(nameTextField.getText().equals(""))
                    inputBox.getChildren().add(3, new StartMenu().getCenterText(
                            "Name field is required", 20));
                else if(nameTextField.getText().length() > 15)
                    inputBox.getChildren().add(3, new StartMenu().getCenterText(
                            "Name cannot be longer than 15 characters", 20));
                else if(genderChoiceBox.getSelectionModel().isEmpty())
                    inputBox.getChildren().add(3, new StartMenu().getCenterText(
                            "You must choose a gender", 20));
            }
        });
        inputBox.getChildren().addAll(nameTextField, genderChoiceBox, submitButton,
                new StartMenu().getCenterText("", 20));

        VBox labelBox = new VBox();
        Label nameLabel = new Label("Name: ");
        nameLabel.setFont(Font.font("Tempus Sans ITC", 35));
        VBox.setMargin(nameLabel, new Insets(0, 0, 35, 0));
        Label genderLabel = new Label("Gender: ");
        genderLabel.setFont(Font.font("Tempus Sans ITC", 35));

        labelBox.getChildren().addAll(nameLabel, genderLabel);

        HBox hBox = new HBox();
        hBox.setMaxSize(600, 250);
        hBox.getChildren().addAll(labelBox, emptyLabel, inputBox);

        return hBox;
    }
}