package Abilities;

public class Ability {

    private int ppMod = 1;

    public Ability() {

    }
    public Ability(int ppMod){
        this.ppMod = ppMod;
    }

    public int getPpMod() {
        return ppMod;
    }

    public void setPpMod(int ppMod) {
        this.ppMod = ppMod;
    }
}
