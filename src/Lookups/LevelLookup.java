package Lookups;

public class LevelLookup {

    public int nextLevelLookup(String expGroup, int gainedExp) {
        int level = levelLookup(expGroup, gainedExp);
        switch (expGroup) {
            case "erratic": {
                if (level == 100)
                    return 0;
                else if (level >= 98)
                    return (int) (((Math.pow(level + 1, 3) * (160 - (level + 1))) / 100.0) -
                            ((Math.pow(level, 3) * (160 - (level))) / 100.0));
                else if (level >= 68)
                    return (int) (((Math.pow(level + 1, 3) * ((1911 - (10 * (level + 1))) / 3.0)) / 500.0) -
                            ((Math.pow(level, 3) * ((1911 - (10 * (level))) / 3.0) / 500.0)));
                else if (level >= 50)
                    return (int) (((Math.pow(level + 1, 3) * (150 - (level + 1))) / 100.0) -
                            ((Math.pow(level, 3) * (150 - (level))) / 100.0));
                else
                    return (int) (((Math.pow(level + 1, 3) * (100 - (level + 1))) / 50.0) -
                            ((Math.pow(level, 3) * (100 - (level))) / 50.0));
            }
            case "fast": {
                if (level == 100)
                    return 0;
                else
                    return (int) ((0.8 * (Math.pow(level + 1, 3) + 1)) - (0.8 * Math.pow(level, 3)));
            }
            case "mediumFast": {
                if (level == 100)
                    return 0;
                else
                    return (int) (Math.pow(level + 1, 3) - Math.pow(level, 3));
            }
            case "mediumSlow": {
                if (level == 100)
                    return 0;
                else
                    return (int) ((1.2 * Math.pow(level + 1, 3) - (15 * Math.pow(level + 1, 2)) + (100 * (level + 1)) - 140) -
                        ((1.2 * Math.pow(level, 3) - (15 * Math.pow(level, 2)) + (100 * level) - 140)));
            }
            case "slow": {
                if (level == 100)
                    return 0;
                else
                    return (int) (1.25 * Math.pow(level + 1, 3) - (1.25 * Math.pow(level, 3)));
            }
            case "fluctuating": {
                if (level == 100)
                    return 0;
                else if (level >= 36)
                    return (int)(Math.pow(level + 1, 3) * (((0.5 * (level + 1)) + 32) / 50) -
                            Math.pow(level, 3) * (((0.5 * level) + 32) / 50));
                else if (level >= 15)
                    return (int)(Math.pow(level + 1, 3) * (((level + 1) + 14) / 50) -
                            Math.pow(level, 3) * ((level + 14) / 50));
                else
                    return (int)(Math.pow(level + 1, 3) * ((((level + 2) / 3.0) + 24) / 50) -
                            Math.pow(level, 3) * ((((level + 1) / 3.0) + 24) / 50));
            }
            default: {
                System.out.println("Error on next level lookup");
                return 0;
            }
        }
    }

    public int levelLookup(String expGroup, int gainedExp) {
        switch (expGroup){
            case "erratic": {
                if (gainedExp > 600000 || gainedExp < 0) {
                    System.out.println("Experience points out of range");
                    return 1;
                }
                else if (gainedExp == 600000)
                    return 100;
                else if (gainedExp >= 378880) {
                    if (gainedExp >= 491346) {
                        if (gainedExp >= 591882)
                            return 99;
                        else if (gainedExp >= 583539)
                            return 98;
                        else if (gainedExp >= 571333)
                            return 97;
                        else if (gainedExp >= 560922)
                            return 96;
                        else if (gainedExp >= 548720)
                            return 95;
                        else if (gainedExp >= 536557)
                            return 94;
                        else if (gainedExp >= 526049)
                            return 93;
                        else if (gainedExp >= 513934)
                            return 92;
                        else if (gainedExp >= 501878)
                            return 91;
                        else
                            return 90;
                    }else {
                        if (gainedExp >= 479378)
                            return 89;
                        else if (gainedExp >= 467489)
                            return 88;
                        else if (gainedExp >= 457001)
                            return 87;
                        else if (gainedExp >= 445239)
                            return 86;
                        else if (gainedExp >= 433572)
                            return 85;
                        else if (gainedExp >= 423190)
                            return 84;
                        else if (gainedExp >= 411686)
                            return 83;
                        else if (gainedExp >= 400293)
                            return 82;
                        else if (gainedExp >= 390077)
                            return 81;
                        else
                            return 80;
                    }
                }else if (gainedExp >= 194400) {
                    if (gainedExp >= 276458) {
                        if (gainedExp >= 276458)
                            return 79;
                        else if (gainedExp >= 357812)
                            return 78;
                        else if (gainedExp >= 346965)
                            return 77;
                        else if (gainedExp >=336255)
                            return 76;
                        else if (gainedExp >= 326531)
                            return 75;
                        else if (gainedExp >= 316074)
                            return 74;
                        else if (gainedExp >= 305767)
                            return 73;
                        else if (gainedExp >= 296358)
                            return 72;
                        else if (gainedExp >= 286328)
                            return 71;
                        else
                            return 70;
                    }else {
                        if (gainedExp >= 267406)
                            return 69;
                        else if (gainedExp >= 257834)
                            return 68;
                        else if (gainedExp >= 249633)
                            return 67;
                        else if (gainedExp >= 241496)
                            return 66;
                        else if (gainedExp >= 233431)
                            return 65;
                        else if (gainedExp >= 225443)
                            return 64;
                        else if (gainedExp >= 217540)
                            return 63;
                        else if (gainedExp >= 209728)
                            return 62;
                        else if (gainedExp >= 202013)
                            return 61;
                        else
                            return 60;
                    }
                }else if (gainedExp >= 76800) {
                    if (gainedExp >= 125000) {
                        if (gainedExp >= 186894)
                            return 59;
                        else if (gainedExp >= 179503)
                            return 58;
                        else if (gainedExp >= 172229)
                            return 57;
                        else if (gainedExp >= 165079)
                            return 56;
                        else if (gainedExp >= 158056)
                            return 55;
                        else if (gainedExp >= 151165)
                            return 54;
                        else if (gainedExp >= 144410)
                            return 53;
                        else if (gainedExp >= 137795)
                            return 52;
                        else if (gainedExp >= 131324)
                            return 51;
                        else
                            return 50;
                    }else {
                        if (gainedExp >= 120001)
                            return 49;
                        else if (gainedExp >= 115015)
                            return 48;
                        else if (gainedExp >= 110052)
                            return 47;
                        else if (gainedExp >= 105122)
                            return 46;
                        else if (gainedExp >= 100237)
                            return 45;
                        else if (gainedExp >= 95406)
                            return 44;
                        else if (gainedExp >= 90637)
                            return 43;
                        else if (gainedExp >= 85942)
                            return 42;
                        else if (gainedExp >= 81326)
                            return 41;
                        else
                            return 40;
                    }
                }else if (gainedExp >= 12800) {
                    if (gainedExp >= 37800) {
                        if (gainedExp >= 72369)
                            return 39;
                        else if (gainedExp >= 68041)
                            return 38;
                        else if (gainedExp >= 63822)
                            return 37;
                        else if (gainedExp >= 59719)
                            return 36;
                        else if (gainedExp >= 55737)
                            return 35;
                        else if (gainedExp >= 51881)
                            return 34;
                        else if (gainedExp >= 48155)
                            return 33;
                        else if (gainedExp >= 44564)
                            return 32;
                        else if (gainedExp >= 41111)
                            return 31;
                        else
                            return 30;
                    } else {
                        if (gainedExp >= 34632)
                            return 29;
                        else if (gainedExp >= 31610)
                            return 28;
                        else if (gainedExp >= 28737)
                            return 27;
                        else if (gainedExp >= 26012)
                            return 26;
                        else if (gainedExp >= 23437)
                            return 25;
                        else if (gainedExp >= 21012)
                            return 24;
                        else if (gainedExp >= 18737)
                            return 23;
                        else if (gainedExp >= 16610)
                            return 22;
                        else if (gainedExp >= 14632)
                            return 21;
                        else
                            return 20;
                    }
                } else {
                    if (gainedExp >= 1800) {
                        if (gainedExp >= 11111)
                            return 19;
                        else if (gainedExp >= 9564)
                            return 18;
                        else if (gainedExp >= 8155)
                            return 17;
                        else if (gainedExp >= 6881)
                            return 16;
                        else if (gainedExp >= 5737)
                            return 15;
                        else if (gainedExp >= 4719)
                            return 14;
                        else if (gainedExp >= 3822)
                            return 13;
                        else if (gainedExp >= 3041)
                            return 12;
                        else if (gainedExp >= 2369)
                            return 11;
                        else
                            return 10;
                    }else {
                        if (gainedExp >= 1326)
                            return 9;
                        else if (gainedExp >= 942)
                            return 8;
                        else if (gainedExp >= 637)
                            return 7;
                        else if (gainedExp >= 406)
                            return 6;
                        else if (gainedExp >= 237)
                            return 5;
                        else if (gainedExp >= 122)
                            return 4;
                        else if (gainedExp >= 52)
                            return 3;
                        else if (gainedExp >= 15)
                            return 2;
                        else
                            return 1;
                    }
                }
            }
            case "fast": {
                if (gainedExp < 800000 && gainedExp >= 0)
                    return (int) Math.pow((1.25 * gainedExp) + 1, (1.0 / 3));
                else {
                    System.out.println("Experience points out of range");
                    return 1;
                }
            }
            case "mediumFast": {
                if (gainedExp <= 1000000 && gainedExp >= 0)
                    return (int) Math.pow(gainedExp + 1, (1.0 / 3));
                else {
                    System.out.println("Experience points out of range");
                    return 1;
                }
            }
            case "mediumSlow": {
                if (gainedExp > 1059860 || gainedExp < 0) {
                    System.out.println("Experience points out of range");
                    return 1;
                }
                else if (gainedExp == 1059860)
                    return 100;
                else if (gainedExp >= 526260) {
                    if (gainedExp >= 762160) {
                        if (gainedExp >= 1027103)
                            return 99;
                        else if (gainedExp >= 995030)
                            return 98;
                        else if (gainedExp >= 963632)
                            return 97;
                        else if (gainedExp >= 932903)
                            return 96;
                        else if (gainedExp >= 902835)
                            return 95;
                        else if (gainedExp >= 873420)
                            return 94;
                        else if (gainedExp >= 844653)
                            return 93;
                        else if (gainedExp >= 816525)
                            return 92;
                        else if (gainedExp >= 789030)
                            return 91;
                        else
                            return 90;
                    }else {
                        if (gainedExp >= 735907)
                            return 89;
                        else if (gainedExp >= 710266)
                            return 88;
                        else if (gainedExp >= 685228)
                            return 87;
                        else if (gainedExp >= 660787)
                            return 86;
                        else if (gainedExp >= 636935)
                            return 85;
                        else if (gainedExp >= 613664)
                            return 84;
                        else if (gainedExp >= 590969)
                            return 83;
                        else if (gainedExp >= 568841)
                            return 82;
                        else if (gainedExp >= 547274)
                            return 81;
                        else
                            return 80;
                    }
                }else if (gainedExp >= 211060) {
                    if (gainedExp >= 344960) {
                        if (gainedExp >= 505791)
                            return 79;
                        else if (gainedExp >= 485862)
                            return 78;
                        else if (gainedExp >= 466464)
                            return 77;
                        else if (gainedExp >= 447591)
                            return 76;
                        else if (gainedExp >= 429235)
                            return 75;
                        else if (gainedExp >= 411388)
                            return 74;
                        else if (gainedExp >= 394045)
                            return 73;
                        else if (gainedExp >= 377197)
                            return 72;
                        else if (gainedExp >= 360838)
                            return 71;
                        else
                            return 70;
                    }else {
                        if (gainedExp >= 329555)
                            return 69;
                        else if (gainedExp >= 314618)
                            return 68;
                        else if (gainedExp >= 300140)
                            return 67;
                        else if (gainedExp >= 286115)
                            return 66;
                        else if (gainedExp >= 272535)
                            return 65;
                        else if (gainedExp >= 259392)
                            return 64;
                        else if (gainedExp >= 246681)
                            return 63;
                        else if (gainedExp >= 234393)
                            return 62;
                        else if (gainedExp >= 222522)
                            return 61;
                        else
                            return 60;
                    }
                }else if (gainedExp >= 56660) {
                    if (gainedExp >= 117360) {
                        if (gainedExp >= 199999)
                            return 59;
                        else if (gainedExp >= 189334)
                            return 58;
                        else if (gainedExp >= 179056)
                            return 57;
                        else if (gainedExp >= 169159)
                            return 56;
                        else if (gainedExp >= 159635)
                            return 55;
                        else if (gainedExp >= 150476)
                            return 54;
                        else if (gainedExp >= 141677)
                            return 53;
                        else if (gainedExp >= 133229)
                            return 52;
                        else if (gainedExp >= 125126)
                            return 51;
                        else
                            return 50;
                    }else {
                        if (gainedExp >= 109923)
                            return 49;
                        else if (gainedExp >= 102810)
                            return 48;
                        else if (gainedExp >= 96012)
                            return 47;
                        else if (gainedExp >= 89523)
                            return 46;
                        else if (gainedExp >= 83335)
                            return 45;
                        else if (gainedExp >= 77440)
                            return 44;
                        else if (gainedExp >= 71833)
                            return 43;
                        else if (gainedExp >= 66505)
                            return 42;
                        else if (gainedExp >= 61450)
                            return 41;
                        else
                            return 40;
                    }
                }else if (gainedExp >= 5460) {
                    if (gainedExp >= 21760) {
                        if (gainedExp >= 52127)
                            return 39;
                        else if (gainedExp >= 47846)
                            return 38;
                        else if (gainedExp >= 43808)
                            return 37;
                        else if (gainedExp >= 40007)
                            return 36;
                        else if (gainedExp >= 36435)
                            return 35;
                        else if (gainedExp >= 33084)
                            return 34;
                        else if (gainedExp >= 29949)
                            return 33;
                        else if (gainedExp >= 27021)
                            return 32;
                        else if (gainedExp >= 24294)
                            return 31;
                        else
                            return 30;
                    } else {
                        if (gainedExp >= 19411)
                            return 29;
                        else if (gainedExp >= 17242)
                            return 28;
                        else if (gainedExp >= 15244)
                            return 27;
                        else if (gainedExp >= 13411)
                            return 26;
                        else if (gainedExp >= 11735)
                            return 25;
                        else if (gainedExp >= 10208)
                            return 24;
                        else if (gainedExp >= 8825)
                            return 23;
                        else if (gainedExp >= 7577)
                            return 22;
                        else if (gainedExp >= 6458)
                            return 21;
                        else
                            return 20;
                    }
                } else {
                    if (gainedExp >= 560) {
                        if (gainedExp >= 4575)
                            return 19;
                        else if (gainedExp >= 3798)
                            return 18;
                        else if (gainedExp >= 3120)
                            return 17;
                        else if (gainedExp >= 2535)
                            return 16;
                        else if (gainedExp >= 2035)
                            return 15;
                        else if (gainedExp >= 1612)
                            return 14;
                        else if (gainedExp >= 1261)
                            return 13;
                        else if (gainedExp >= 973)
                            return 12;
                        else if (gainedExp >= 742)
                            return 11;
                        else
                            return 10;
                    }else {
                        if (gainedExp >= 419)
                            return 9;
                        else if (gainedExp >= 314)
                            return 8;
                        else if (gainedExp >=236)
                            return 7;
                        else if (gainedExp >= 179)
                            return 6;
                        else if (gainedExp >= 135)
                            return 5;
                        else if (gainedExp >= 96)
                            return 4;
                        else if (gainedExp >= 57)
                            return 3;
                        else if (gainedExp >= 9)
                            return 2;
                        else
                            return 1;
                    }
                }
            }
            case "slow": {
                if (gainedExp <= 1250000 && gainedExp >= 0)
                    return (int) Math.pow((0.8 * gainedExp) + 1, (1.0 / 3));
                else {
                    System.out.println("Experience points out of range");
                    return 1;
                }
            }
            case "fluctuating": {
                if (gainedExp > 1640000 || gainedExp < 0) {
                    System.out.println("Experience points out of range");
                    return 1;
                }
                else if (gainedExp == 1640000)
                    return 100;
                else if (gainedExp >= 737280) {
                    if (gainedExp >= 1122660) {
                        if (gainedExp >= 1571884)
                            return 99;
                        else if (gainedExp >= 1524731)
                            return 98;
                        else if (gainedExp >= 1460276)
                            return 97;
                        else if (gainedExp >= 1415577)
                            return 96;
                        else if (gainedExp >= 1354652)
                            return 95;
                        else if (gainedExp >= 1312322)
                            return 94;
                        else if (gainedExp >= 1254796)
                            return 93;
                        else if (gainedExp >= 1214753)
                            return 92;
                        else if (gainedExp >= 1160499)
                            return 91;
                        else
                            return 90;
                    }else {
                        if (gainedExp >= 1071552)
                            return 89;
                        else if (gainedExp >= 1035837)
                            return 88;
                        else if (gainedExp >= 987754)
                            return 87;
                        else if (gainedExp >= 954084)
                            return 86;
                        else if (gainedExp >= 908905)
                            return 85;
                        else if (gainedExp >= 877201)
                            return 84;
                        else if (gainedExp >= 834809)
                            return 83;
                        else if (gainedExp >= 804997)
                            return 82;
                        else if (gainedExp >= 765275)
                            return 81;
                        else
                            return 80;
                    }
                }else if (gainedExp >= 267840) {
                    if (gainedExp >= 459620) {
                        if (gainedExp >= 700115)
                            return 79;
                        else if (gainedExp >= 673863)
                            return 78;
                        else if (gainedExp >= 639146)
                            return 77;
                        else if (gainedExp >= 614566)
                            return 76;
                        else if (gainedExp >= 582187)
                            return 75;
                        else if (gainedExp >= 559209)
                            return 74;
                        else if (gainedExp >= 529063)
                            return 73;
                        else if (gainedExp >= 507617)
                            return 72;
                        else if (gainedExp >= 479600)
                            return 71;
                        else
                            return 70;
                    }else {
                        if (gainedExp >= 433631)
                            return 69;
                        else if (gainedExp >= 415050)
                            return 68;
                        else if (gainedExp >= 390991)
                            return 67;
                        else if (gainedExp >= 373744)
                            return 66;
                        else if (gainedExp >= 351520)
                            return 65;
                        else if (gainedExp >= 335544)
                            return 64;
                        else if (gainedExp >= 315059)
                            return 63;
                        else if (gainedExp >= 300293)
                            return 62;
                        else if (gainedExp >= 281456)
                            return 61;
                        else
                            return 60;
                    }
                }else if (gainedExp >= 66560) {
                    if (gainedExp >= 142500) {
                        if (gainedExp >=250562)
                            return 59;
                        else if (gainedExp >= 238036)
                            return 58;
                        else if (gainedExp >= 222231)
                            return 57;
                        else if (gainedExp >= 210739)
                            return 56;
                        else if (gainedExp >= 196322)
                            return 55;
                        else if (gainedExp >= 185807)
                            return 54;
                        else if (gainedExp >= 172697)
                            return 53;
                        else if (gainedExp >= 163105)
                            return 52;
                        else if (gainedExp >= 151222)
                            return 51;
                        else
                            return 50;
                    }else {
                        if (gainedExp >= 131766)
                            return 49;
                        else if (gainedExp >= 123863)
                            return 48;
                        else if (gainedExp >= 114205)
                            return 47;
                        else if (gainedExp >= 107069)
                            return 46;
                        else if (gainedExp >= 98415)
                            return 45;
                        else if (gainedExp >= 91998)
                            return 44;
                        else if (gainedExp >= 84277)
                            return 43;
                        else if (gainedExp >= 78533)
                            return 42;
                        else if (gainedExp >= 71677)
                            return 41;
                        else
                            return 40;
                    }
                }else if (gainedExp >= 5440) {
                    if (gainedExp >=  23760) {
                        if (gainedExp >= 60505)
                            return 39;
                        else if (gainedExp >= 55969)
                            return 38;
                        else if (gainedExp >= 50653)
                            return 37;
                        else if (gainedExp >= 46656)
                            return 36;
                        else if (gainedExp >= 42017)
                            return 35;
                        else if (gainedExp >= 37731)
                            return 34;
                        else if (gainedExp >= 33780)
                            return 33;
                        else if (gainedExp >= 30146)
                            return 32;
                        else if (gainedExp >= 26811)
                            return 31;
                        else
                            return 30;
                    } else {
                        if (gainedExp >= 20974)
                            return 29;
                        else if (gainedExp >= 18439)
                            return 28;
                        else if (gainedExp >= 16140)
                            return 27;
                        else if (gainedExp >= 14060)
                            return 26;
                        else if (gainedExp >= 12187)
                            return 25;
                        else if (gainedExp >= 10506)
                            return 24;
                        else if (gainedExp >= 9003)
                            return 23;
                        else if (gainedExp >= 7666)
                            return 22;
                        else if (gainedExp >= 6482)
                            return 21;
                        else
                            return 20;
                    }
                } else {
                    if (gainedExp >= 540) {
                        if (gainedExp >= 4526)
                            return 19;
                        else if (gainedExp >= 3732)
                            return 18;
                        else if (gainedExp >= 3046)
                            return 17;
                        else if (gainedExp >= 2457)
                            return 16;
                        else if (gainedExp >= 1957)
                            return 15;
                        else if (gainedExp >= 1591)
                            return 14;
                        else if (gainedExp >= 1230)
                            return 13;
                        else if (gainedExp >= 967)
                            return 12;
                        else if (gainedExp >= 745)
                            return 11;
                        else
                            return 10;
                    }else {
                        if (gainedExp >= 393)
                            return 9;
                        else if (gainedExp >= 276)
                            return 8;
                        else if (gainedExp >= 178)
                            return 7;
                        else if (gainedExp >= 112)
                            return 6;
                        else if (gainedExp >= 65)
                            return 5;
                        else if (gainedExp >= 32)
                            return 4;
                        else if (gainedExp >= 13)
                            return 3;
                        else if (gainedExp >= 4)
                            return 2;
                        else
                            return 1;
                    }
                }
            }
            default: {
                System.out.println("Error on level lookup for: " + expGroup);
                return 1;
            }
        }
    }
    public int getMaxExp(String expGroup) {
        switch(expGroup) {
            case "erratic":
                return 600000;
            case "fast":
                return 800000;
            case "mediumFast":
                return 1000000;
            case "mediumSlow":
                return 1059860;
            case "slow":
                return 1250000;
            case "fluctuating":
                return 1640000;
            default: {
                System.out.println("Invalid experience group: " + expGroup);
                return 0;
            }
        }
    }
}