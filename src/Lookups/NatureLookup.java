package Lookups;

public class NatureLookup {

    public double getNatureModifier(String nature, String stat) {
        String natures = "hardy lonely brave adamant naughty bold docile relaxed impish" +
                "lax timid hasty serious jolly naive modest mild quiet bashful rash calm gentle" +
                "sassy careful quirky";
        String stats = "attack defense spAtk spDef speed";
        if(!natures.contains(nature) || !stats.contains(stat)) {
            System.out.println("getNatureModifier: input error");
            return 1;
        }

        switch (nature) {
            case "lonely": {
                switch (stat) {
                    case "attack":
                        return 1.1;
                    case "defense":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "brave": {
                switch (stat) {
                    case "attack":
                        return 1.1;
                    case "speed":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "adamant": {
                switch (stat) {
                    case "attack":
                        return 1.1;
                    case "spAtk":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "naughty": {
                switch (stat) {
                    case "attack":
                        return 1.1;
                    case "spDef":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "bold": {
                switch (stat) {
                    case "defense":
                        return 1.1;
                    case "attack":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "relaxed": {
                switch (stat) {
                    case "defense":
                        return 1.1;
                    case "speed":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "impish": {
                switch (stat) {
                    case "defense":
                        return 1.1;
                    case "spAtk":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "lax": {
                switch (stat) {
                    case "defense":
                        return 1.1;
                    case "spDef":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "timid": {
                switch (stat) {
                    case "speed":
                        return 1.1;
                    case "attack":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "hasty": {
                switch (stat) {
                    case "speed":
                        return 1.1;
                    case "defense":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "jolly": {
                switch (stat) {
                    case "speed":
                        return 1.1;
                    case "spAtk":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "naive": {
                switch (stat) {
                    case "speed":
                        return 1.1;
                    case "spDef":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "modest": {
                switch (stat) {
                    case "spAtk":
                        return 1.1;
                    case "attack":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "mild": {
                switch (stat) {
                    case "spAtk":
                        return 1.1;
                    case "defense":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "quiet": {
                switch (stat) {
                    case "spAtk":
                        return 1.1;
                    case "speed":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "rash": {
                switch (stat) {
                    case "spAtk":
                        return 1.1;
                    case "spDef":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "calm": {
                switch (stat) {
                    case "spDef":
                        return 1.1;
                    case "attack":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "gentle": {
                switch (stat) {
                    case "spDef":
                        return 1.1;
                    case "defense":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "sassy": {
                switch (stat) {
                    case "spDef":
                        return 1.1;
                    case "speed":
                        return 0.9;
                    default:
                        return 1;
                }
            }case "careful": {
                switch (stat) {
                    case "spDef":
                        return 1.1;
                    case "spAtk":
                        return 0.9;
                    default:
                        return 1;
                }
            }
            default:
                return 1;
        }
    }
}
